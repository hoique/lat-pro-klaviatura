# Latviešu programmētāju klaviatūras izkārtojums
Nodrošina sakarīgu QWERTY izkārtojumu klaviatūrai ar latviešu valodas diakritiskajām zīmēm bez parastā Windows latviešu klaviatūras izkārtojuma "dead key" funkcionalitātes, kas traucē apostrofu ievadei.

